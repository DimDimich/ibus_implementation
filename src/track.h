#ifndef _TRACK_H_
#define _TRACK_H_

#include "mbed.h"
#include "inttypes.h"

#include "ibus.h"

class Track {
    PwmOut left_f;
    PwmOut left_b;
    PwmOut right_f;
    PwmOut right_b;
public:
    Track(
        PinName left_f_p,
        PinName left_b_p,
        PinName right_f_p,
        PinName right_b_p
    );

    void update(const FlySkyRx &fsrx);
};

#endif // _TRACK_H_
