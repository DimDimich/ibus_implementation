#ifndef _FS_IBUS_H_
#define _FS_IBUS_H_

#include "inttypes.h"

#define BUFFSIZE 32
#define SYNCBYTE 0x20
#define CHCKSUM_DEFAULT 0xFFFF

enum FS_CHANNELS{
    ROLL=1,
    PITCH,
    THROTTLE,
    YAW,
    SWA,
    SWB,
    VRA,
    VRB,
    SWC,
    SWD
};

class FlySkyRx {
public:
    bool ready;

    FlySkyRx(): position(0) {}

    uint16_t const& operator[](FS_CHANNELS ch) const {
        return channels[ch];
    }

    // @param v: nextByte
    void processNextByte(uint8_t value);

private:
    uint8_t position;
    uint16_t chksum;
    uint16_t rxsum;
    uint16_t channels[16];
};


#endif // _FS_IBUS_H_
