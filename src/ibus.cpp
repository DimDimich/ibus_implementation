#include "ibus.h"

void FlySkyRx::processNextByte(uint8_t value) {
    if (position == 0 && value != SYNCBYTE) {
        return;
    }

    uint8_t *data = reinterpret_cast<uint8_t*>(channels);
    ready = false;

    data[position] = value;

    if (position == BUFFSIZE - 1) {

        chksum = CHCKSUM_DEFAULT;
        rxsum = 0;
        // Calculate checksum
        for (uint8_t i = 0; i < BUFFSIZE - 2; i++) {
            chksum -= data[i];
        }

        // Get checksum from received data
        rxsum = data[30] | (data[31] << 8);

        // Compare calculated checksum and received one
        if (rxsum == chksum) {
            ready = true;
        }

        position = 0;
    } else {
        position++;
    }
}
