/*
    Simple implementation of IBus protocol
    Transmitter FS-i6
    Receiver FS-iA6B
    Bridged motor driver L298N
    Tracked chassis

    TODO: Refactor code to make usage more convenient
    TODO: Optimize resource consumption
*/

#include "mbed.h"
#include "inttypes.h"
#include "ibus.h"
#include "track.h"

uint8_t getNextByte(RawSerial &fs) {
    while(!fs.readable()) {
    }
    return fs.getc();
}

int main() {
    RawSerial fss(NC, PA_10, 115200);

    FlySkyRx fsrx;
    Track track(PB_4, PB_6, PB_10, PA_7);

    while(1) {
        fsrx.processNextByte(getNextByte(fss));
        if(fsrx.ready) {
            track.update(fsrx);
        }
    }
}
