#include "track.h"

Track::Track(
    PinName left_f_p,
    PinName left_b_p,
    PinName right_f_p,
    PinName right_b_p
):
    left_f(left_f_p),
    left_b(left_b_p),
    right_f(right_f_p),
    right_b(right_b_p)
{
    left_f.pulsewidth_ms(50);
    left_b.pulsewidth_ms(50);
    right_f.pulsewidth_ms(50);
    right_b.pulsewidth_ms(50);

    left_f.write(0);
    left_b.write(0);
    right_f.write(0);
    right_b.write(0);
}

void Track::update(const FlySkyRx &fsrx) {
    if (fsrx[SWA] > 1500) {
        // convert range from 1000:2000 to 0.0f:1.0f
        float t = (fsrx[THROTTLE] - 1000) / 1000.f;

        // convert range from 1000:2000 to -0.5f:0.5f
        // 0.0f = forward
        // -0.5f = left
        // 0.5f = right
        float y = (fsrx[YAW] - 1500) / 1000.f;


        float ltrack = t + y;
        float rtrack = t - y;

        if (ltrack >= 0) {
            left_f.write(ltrack);
            left_b.write(0);
        } else {
            left_f.write(0);
            left_b.write(-ltrack);
        }

        if (rtrack >= 0) {
            right_f.write(rtrack);
            right_b.write(0);
        } else {
            right_f.write(0);
            right_b.write(-rtrack);
        }
    } else {
        left_f.write(0);
        left_b.write(0);
        right_f.write(0);
        right_b.write(0);
    }
}
